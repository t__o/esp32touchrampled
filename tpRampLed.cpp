

#include "Arduino.h"



// *********)()()())))))))------	 LED Strip

#include <WS2812FX.h>

#define STRIPLED_COUNT 30
#define STRIPLED_PIN 22
// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
WS2812FX ws2812fx = WS2812FX(STRIPLED_COUNT, STRIPLED_PIN, NEO_RGB + NEO_KHZ800);
byte ledModeCount;

// ---------- changeAuto
bool changeAutoOn = false;
int  changeAutoDelay = 30000;
unsigned long  changeAutoTimer ;




unsigned long  now ;

// led blink
#define LED_PIN BUILTIN_LED
unsigned long  ledTimer ;
bool ledisOn ;
int ledBlinkDuration = 100;
byte ledCounter   = 0;


class RampC{

	/*
	 * Per touchpad
	 *
	 *     1x   increment 1
	 *     2x	inverse direction
	 *     long ramp  ( if stay cliked longer than long, start ramping)
	 *
	 **/

	// parameters
	String  name;
	byte pin;						// pin, use T9 T8 ... (this translate to real gpio)
	int rampMin ;					// min value
	int rampMax ;					// max value
	int rampStep ;					// setp value

	// state
	int rampValue  = 0 ;			// rampValue
	bool rampDirUp = false;			// Direction of the ramp

	// touchpad
	int tp_level = 0;				// activation if current is lower
	const float tp_lower_level = 0.7;	// ratio to calculate touch limit

	bool tp_state = false;			// state of the toggle
	bool debounceOn = false;		// debounce is active toggle
	bool isLong =false;

	unsigned long tp_timer=0 ;		// timer of the touch
	unsigned long tp_dbtimer=0 ;	// timer of the touch
	unsigned long tp_rampTimer=0;	// Ramp Timer
	unsigned long releaseTimer = 0;

	byte tp_pushCount =0;
	int rampCount = 0;

	// Click delays
	const int debounceDelay = 15;	// Debounce protection min of Push
	const int shortClickDelay = 50;	// Minimal push duration
	const int rampStepDelay = 100;	// Duration to step ramping
	const int rampStartDelay = 700;	// Duration to step ramping
	const int releaseDelay = 500 ;  // max delay between 2 push
	const int pushLongDelay = 850;	// Count long push after

	// count the push  for exetrnal use
	int lastPushCount = 0 ;

public:
	//RampC();
	RampC(	byte attachTo,
			String pname,
			int pmin,
			int pmax,
			int pstep
	) {
		pin = attachTo;
		name= pname;
		rampMin = pmin ;
		rampMax = pmax ;
		rampStep = pstep ;
	}

	void setInit(int pvalue){
		// set Current value
		rampValue = pvalue ;
		int initVal =  read();
		tp_level = initVal*tp_lower_level;
		Serial.println(   String(name) + " " + String(tp_level) );
		tp_state = false;
	}

	int get(){
		return rampValue;
	}
	void set( int value){
		rampValue = value ;
	}

	int getPushCount(){
		return lastPushCount;
	}


	bool update(unsigned long now){

		// --- After debounced toggle is validated : set the Push / Release
		if ( debounce(now)) {

			// -- get the open / release of push
			if (tp_state ){										// start a push
				//Serial.print( name +	" PO  " );
				tp_timer = now;									// set timer and debounce
				tp_rampTimer = now ;
				return false;
			}else{												// release a push
				int duration = now - tp_timer ;
				if (duration < shortClickDelay  ){				// reset short
					tp_state = false;
					return false;
				}
				//Serial.println( name +	" PC  " + String( duration));
				isLong = duration > pushLongDelay  ;
				// set the releaseTimer and pushCount
				releaseTimer = now ;
				tp_pushCount++;
				return false;  // wait for timer ?
			}
		}

		//  Ramp if Push is on
		if (tp_state ){
			if (now - tp_timer > rampStartDelay){				// wait for ramp start
				if(now-tp_rampTimer > rampStepDelay ){
					tp_rampTimer =  now;  							// reset ramp timer
					//				Serial.print(name +  " PRamp "+
					//						String(now -  tp_timer)+
					//						String(now -  tp_rampTimer)+
					//						"\n");
					// same as send 1 click
					return rampChange( 1 );;
				}
			}
		}

		// --- after release and delay count the push
		if ( ! tp_state && tp_pushCount >0){
			// wait for delay between 2 pushes
			if ( now - releaseTimer > releaseDelay){

				Serial.print( name  +  " Push Count ");
				Serial.print(tp_pushCount );
				Serial.print(isLong ? " Long":" Short");
				Serial.print("\n");

				lightLED();

				// Keep the value for external use
				lastPushCount = tp_pushCount;
				// reset count
				tp_pushCount = 0;
				return  rampChange( lastPushCount );								// true on update
			}
		}
		return false;
	}

protected:

	int read (){
		return touchRead(pin);
	}

	void lightLED(){
		// light LED with timer
		//digitalWrite(LED_PIN, HIGH);
		ledTimer = now;
		ledisOn = true;
		ledCounter = tp_pushCount *2  ;
	}

	/**
	 * return true if click occured
	 */
	bool debounce(unsigned long now){

		int touch_value = read();
		if (touch_value == 0 )  return false;

		bool readState = touch_value <= tp_level ;				// read value state

		if (  ! debounceOn && readState != tp_state){			// if it changed and we are not in a debounce
			tp_dbtimer = now;									// set timer and debounce
			debounceOn = true;
		}
		if ( debounceOn && now -tp_dbtimer > debounceDelay ){	// when debounce timer released
			tp_state =  ! tp_state;								// change the state
			debounceOn = false;
			//Serial.println( name +	" tp_state  "+ String(tp_state?" on ":" off") );
			return true;
		}
		return false;
	}



	/**
	 * Change the ramp Value based on click
	 */
	bool rampChange( byte clicks){


		int prevValue = rampValue;

		switch (clicks){
		case 1 :						// Increment (1 push and ramp)
			rampValue =  rampDirUp ?  rampValue +rampStep : rampValue - rampStep;
			break ;
		case 2 :						// Switch Direction
			rampDirUp  = !  rampDirUp ;
			Serial.println( name + " direction  "+(rampDirUp ? " up " : " down " )) ;

			break ;
		}


		// Apply Limits
		if ( rampValue < rampMin ) rampValue = rampMin;
		if ( rampValue >rampMax ) rampValue = rampMax;

		//Serial.println(   name +" current "+ String(rampValue)  ) ;

		return rampValue != prevValue ;
	}

} ;


// Create objects Ramps
RampC cSpeed (T5,"speed",1,3000,10);
RampC cBright (T7,"briht",1,100,1);
RampC cMode (T9,"mode",1,60,1);


void setupStrip( int speed, int bright, int mode) {
	ws2812fx.init();
	ws2812fx.setBrightness(bright);
	ws2812fx.setSpeed(speed);
	ws2812fx.setColor(0x007BFF);
	ws2812fx.setMode(FX_MODE_STATIC);
	ws2812fx.start();
	Serial.println( "... LED Started");
	ledModeCount = ws2812fx.getModeCount();
	ws2812fx.setMode( mode );
}



void changeAuto(){
	// change on delay
	if ( changeAutoOn ){
		if (now - changeAutoTimer > changeAutoDelay){
			changeAutoTimer = now;
			// incrment mode with limits
			cMode.set( cMode.get()+1);
			if ( cMode.get() < 20 ) cMode.set(20);
			if ( cMode.get() > ledModeCount-1 )cMode.set( ledModeCount-1 );
			//ws2812fx.setMode( ledMode );
			ws2812fx.setMode(cMode.get());
			Serial.println( "Mode " + String(ws2812fx.getModeName(cMode.get())) +
					" i " +String(cMode.get()) +
					" c " + String(ledModeCount)
			);
		}
	}
}


/**@brief Function setup touch pad pins
 */
void setup() {
	Serial.begin(115200);
	Serial.println("Starting " );

	// LED
	pinMode(LED_PIN, OUTPUT);
	digitalWrite(LED_PIN, LOW);   // turn the LED off

	// setup the ramps
	cSpeed.setInit(100);
	cBright.setInit(2);
	cMode.setInit(33);

	// setup ledstrip
	setupStrip( cSpeed.get(),cBright.get(),cMode.get());
}


/**@brief Function loop to read touch pad pins
 */
void loop() {
	now = millis();

	ws2812fx.service();

	if (cSpeed.update(now)){
		Serial.println( " speed updated " + String( cSpeed.get()));
		ws2812fx.setSpeed(cSpeed.get());
	};
	if (cBright.update(now)){
		Serial.println( " cBright updated " + String( cBright.get()));
		ws2812fx.setBrightness(cBright.get());
	};
	if (cMode.update(now)){
		if (cMode.getPushCount()==4){
			changeAutoOn = ! changeAutoOn;
			Serial.println( "Change Auto is : " + String (changeAutoOn? " ON ":" OFF "  ));
		}else if (cMode.getPushCount()==1){
			Serial.println( " cMode updated " + String( cMode.get()));
			ws2812fx.setMode(cMode.get());
		}
	};

}
